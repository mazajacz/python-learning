
# a = 2 * 'test'
# assert(a == 'testtest')
# a = 1.2
# assert(type(a) == float)




def moja_funkcja(a,b):
    try:
        a = int(a)
        return a/b
    except ValueError as err:
        print(f"err: {err} ")
    except Exception as exc:
        print(f"exc test: {exc}")
    return None

assert (moja_funkcja(100,0) == None)

assert (moja_funkcja('test',2) == 'testtest' )

assert (moja_funkcja(1,2) == 2 )

assert (moja_funkcja(1,'0') == '0')

res =  moja_funkcja('dwa', -1)

assert (res == '')

assert (moja_funkcja(1,1.1) == 1 )
