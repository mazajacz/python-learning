import unittest
from parameterized import parameterized

my_test_case = [["a","1", "1"], ["b", "-1", "2"], ["c", "2", "2"]]

class TestSequence(unittest.TestCase):
    @parameterized.expand(my_test_case)
    def test_sequence(self,name, a, b):
        self.assertEqual(a, b)

if __name__ == '__main__':
    unittest.main()