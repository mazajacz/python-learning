class Descriptor(object):

    def get(self, instance, owner):
        print
        "Getting: %s" % self.name
        return self.name

    def _set(self, instance, name):
        print
        "Setting: %s" % name
        self._name = name.title()

    def __delete(self, instance):
        print
        "Deleting: %s" % self._name
        del self._name


class Person(object):
    name = Descriptor()

p = Person()
d = Descriptor()

d.name = 'testowe imie'
print(d.name)
p.name = 'testjowe imie'
print(p.name)
del(p.name)
print(p.name)