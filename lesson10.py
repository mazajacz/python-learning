def double_chars_repeater(text):
    result = ''
    double_letter = ''
    single_letter = ''
    list_from_text = list(text)
    for index, value in enumerate(list_from_text):
        # print("I: {} V: {}".format(index,value))
        if index % 2 == 1:
            # print("Index % 1: {} ".format(value * 2))
            result += double_letter.join(value * 2)
        else:
            # print("Index % 0: {} ".format(value))
            result += single_letter.join(value) 
    # print(result)
    return result
  
 
assert '' == double_chars_repeater('')
assert 'ALLA__MAA PPSAA' == double_chars_repeater('ALA_MA PSA')
assert '122344566' == double_chars_repeater('123456')
assert '+--+--+' == double_chars_repeater('+-+-+')