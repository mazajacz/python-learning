import random

class LotteryArgumentException(Exception):
    pass

def lottery(min_value = 1,max_value = 3):
    if min_value <= 0:
        raise LotteryArgumentException('Required min value <= 0')
    random_number = random.randint(min_value,max_value)
    print('random_number: {}'.format(random_number))
    return random_number

def quasy_list(count):
    return ([1,2] * count)

#assert (lottery(1,2) > 0 )
try:
    lottery(0,1)
    assert(False)
except LotteryArgumentException:
    assert(True)