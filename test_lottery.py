import unittest
from lesson02 import lottery,LotteryArgumentException,quasy_list

class LotteryTest(unittest.TestCase):
    def test_lottery(self):
        lottery(1, 4)
        self.assertTrue(True)

    def test_should_not_lottery(self):
        with self.assertRaises(LotteryArgumentException) as context:
            lottery(0,1)

    def test_should_not_lottery_with_minus(self):
        with self.assertRaises(LotteryArgumentException) as context:
            lottery(-1, 2)

    def test_quasy_list(self):
        result = quasy_list(0)
        self.assertIsInstance(result,list)
        self.assertTrue(len(result) > 0)
        self.assertEqual([1,2],result)